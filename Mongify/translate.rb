table "pos_orders" do
  column "id", :key
  column "title", :string
  column "owner_id", :integer, :references => :users
  column "body", :text
  column "published_at", :datetime
  column "created_at", :datetime
  column "updated_at", :datetime
end


CREATE TABLE "pos_orders" (
  "id", :key
  "bar_id", : integer
  "code" ,:string
  "date", : datetime
  "nd" date NOT NULL,
  "hour" int(11) NOT NULL,
  "promo1_id" int(11) NOT NULL,
  "promo2_id" int(11) NOT NULL,
  "promo3_id" int(11) NOT NULL,
  "discount" decimal(10,2) NOT NULL DEFAULT '0.00',
  "tips" decimal(10,2) NOT NULL DEFAULT '0.00',
  "other_charges" decimal(10,2) NOT NULL DEFAULT '0.00',
  "total_taxable_sales" decimal(10,2) NOT NULL,
  "total_untaxable_sales" decimal(10,2) NOT NULL,
  "taxes" decimal(10,2) DEFAULT '0.00',
  "total_sales" decimal(10,2) DEFAULT NULL,
  "total_sales_inc_tax" decimal(10,2) DEFAULT NULL,
  "payment_type" varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  "seats" int(11) NOT NULL,
  "table_number" varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  "fiscal_date" date NOT NULL,
  "time_open" datetime DEFAULT NULL,
  "time_ordered" datetime DEFAULT NULL,
  "time_close" datetime DEFAULT NULL,
  "time_send" datetime DEFAULT NULL,
  "time_print" datetime DEFAULT NULL,
  "time_transaction" datetime DEFAULT NULL,
  "employee_id" int(11) DEFAULT NULL,
  "waiter" varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  "terminal" varchar(100) DEFAULT NULL,
  "pulled_at" datetime DEFAULT CURRENT_TIMESTAMP,
  "created_date" datetime DEFAULT CURRENT_TIMESTAMP,
  "modified_at" datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  "data_source" int(11) DEFAULT NULL,
  "waste_date" date DEFAULT NULL,
  "status_id" int(11) DEFAULT '0',
  "origin_id" bigint(11) unsigned DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "bar_id" ("bar_id"),
  KEY "nd" ("nd"),
  KEY "barid_nd" ("bar_id","nd"),
  KEY "modified_at" ("modified_at"),
  KEY "idx_pulled_at" ("pulled_at"),
  KEY "idx_createdate" ("created_date")
) ENGINE=InnoDB AUTO_INCREMENT=44830635 DEFAULT CHARSET=latin1;


table "pos_ordersItems", :embed_in => :pos_orders, :on => :post_id do
  column "id", :key
  column "body", :text
  column "post_id", :integer, :referneces => :posts
  column "user_id", :integer, :references => :users
  column "created_at", :datetime
  column "updated_at", :datetime
end

table "preferences", :embed_in => :users, :as => :object do
  column "id", :key
  column "user_id", :integer, :references => "users"
  column "notify_by_email", :boolean
end

table "notes", :embed_in => true, :polymorphic => 'notable' do
  column "id", :key
  column "user_id", :integer, :references => "users"
  column "notable_id", :integer
  column "notable_type", :string
  column "body", :text
  column "created_at", :datetime
  column "updated_at", :datetime
end
