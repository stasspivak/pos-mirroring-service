package com.weissbeerger.threads;

import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

@Component
public class DaemonThreadFactory implements ThreadFactory {

    ThreadFactory defaultFactory = Executors.defaultThreadFactory();

    public Thread newThread(Runnable runnable) {
        Thread newThread = defaultFactory.newThread(runnable);
        newThread.setDaemon(true);
        return newThread;
    }
}
