package com.weissbeerger.criteria;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class MirroringCriteria {

    @JsonFormat
    private LocalDate lastDate;
    @JsonFormat
    private Integer idStart;
    @JsonFormat
    private Integer idEnd;

}
