package com.weissbeerger.config;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Connection;
import java.sql.SQLException;

@Configuration
@ConfigurationProperties
public class DatabaseConfig {

    private static final String CONNECTION_TEST_QUERY = "SELECT 1";


    private HikariConfig config = new HikariConfig();
    private HikariDataSource ds;

    @Value("${hikari.mysql.jdbc-url}")
    private String mySqlJdbcUrl;

    @Value("${hikari.mysql.username}")

    private String mySqlUserName;

    @Value("${hikari.mysql.password}")
    private String mySqlPassword;

    @Value("${hikari.mysql.pool.size}")
    private int mySqlPoolSize;

    // MySQL Datasource initialization
    @Bean
    public HikariDataSource mySqlHikariDataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(mySqlJdbcUrl);
        hikariDataSource.setUsername(mySqlUserName);
        hikariDataSource.setPassword(mySqlPassword);
        hikariDataSource.setMaximumPoolSize(mySqlPoolSize);
        hikariDataSource.setConnectionTestQuery(CONNECTION_TEST_QUERY);
        hikariDataSource.setPoolName("MySQL-HikariPool");
        return hikariDataSource;
    }


    public DatabaseConfig() {

    }

    @Bean(name = "mySqlNamedParameterJdbcTemplate")
    @Primary
    public NamedParameterJdbcTemplate mySqlNamedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(mySqlHikariDataSource());
    }

    public Connection getConnection() throws SQLException {

        return mySqlHikariDataSource().getConnection();
    }

}