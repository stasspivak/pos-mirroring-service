package com.weissbeerger.config;

import com.mongodb.MongoClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;

@Configuration
@ConfigurationProperties
public class MongoConfig {

    MongoClient mongo = null;

    public MongoClient getMongo() {
        return mongo;
    }

    @Bean
    public MongoClient mongoClient() throws UnknownHostException {
        mongo = new MongoClient( "localhost" , 27017 );
        return mongo;
    }
}
