package com.weissbeerger.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {
    protected static final DateTimeFormatter FORMAT_WITH_FRACTIONS =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss[.S]");

    public static LocalDateTime getLocalDateTime(String timeStr) {
        return LocalDateTime.parse(timeStr, FORMAT_WITH_FRACTIONS);
    }
}
