package com.weissbeerger.mappers;

import com.weissbeerger.model.BarProduct;
import com.weissbeerger.model.PosProduct;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class PosProductMapper implements RowMapper<PosProduct> {

    public PosProduct mapRow(ResultSet rs,int rowNum) throws SQLException {
        PosProduct product = new PosProduct();
        product.setId(rs.getLong("id"));
        product.setTitle(rs.getString("title"));
        product.setSku(rs.getString("sku"));
        product.setBrandId(rs.getLong("brand_id"));
        product.setServingSizeId(rs.getLong("serving_size_id"));
        product.setServingTypeId(rs.getInt("serving_type_id"));
        product.setPackageSize(rs.getString("package_size"));

        return product;
    }
}
