package com.weissbeerger.mappers;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.weissbeerger.model.Order;
import com.weissbeerger.model.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderRowMapper implements RowMapper<Order>{

    @Autowired
    RowMapper<OrderItem> orderItemRowMapper;

    public Order mapRow(ResultSet rs,int rowNum) throws SQLException {

        Order order = new Order();
        order.setId(rs.getLong("id"));
        order.setBarId(rs.getInt("bar_id"));
        order.setCode(rs.getString("code"));
        if (rs.getTimestamp("date") != null) {
            order.setDate(rs.getTimestamp("date").toLocalDateTime());
        }
        if (rs.getDate("nd") != null) {
            order.setNd(rs.getDate("nd").toLocalDate());
        }
        order.setHour(rs.getInt("hour"));
        order.setPromo1Id(rs.getInt("promo1_id"));
        order.setPromo2Id(rs.getInt("promo2_id"));
        order.setPromo3Id(rs.getInt("promo3_id"));
        order.setDiscount(rs.getDouble("discount"));
        order.setTips(rs.getDouble("tips"));
        order.setOtherCharges(rs.getDouble("other_charges"));
        order.setTotalTaxableSales(rs.getDouble("total_taxable_sales"));
        order.setTotalUntaxableSales(rs.getDouble("total_untaxable_sales"));
        order.setTaxes(rs.getDouble("taxes"));
        order.setTotalSales(rs.getDouble("total_sales"));
        order.setTotalSalesIncTax(rs.getDouble("total_sales_inc_tax"));
        order.setPaymentType(rs.getString("payment_type"));
        order.setSeats(rs.getInt("seats"));
        order.setTableNumber(rs.getString("table_number"));
        if (rs.getDate("fiscal_date") != null) {
            order.setFiscalDate(rs.getDate("fiscal_date").toLocalDate());
        }
        if (rs.getTimestamp("time_open") != null) {
            order.setTimeOpen(rs.getTimestamp("time_open").toLocalDateTime());
        }
        if (rs.getTimestamp("time_ordered") != null) {
            order.setTimeOrdered(rs.getTimestamp("time_ordered").toLocalDateTime());
        }
        if (rs.getTimestamp("time_close") != null) {
            order.setTimeClose(rs.getTimestamp("time_close").toLocalDateTime());
        }
        if (rs.getTimestamp("time_send") != null) {
            order.setTimeSend(rs.getTimestamp("time_send").toLocalDateTime());
        }
        if (rs.getTimestamp("time_print") != null) {
            order.setTimePrint(rs.getTimestamp("time_print").toLocalDateTime());
        }
        if (rs.getTimestamp("time_transaction") != null) {
            order.setTimeTransaction(rs.getTimestamp("time_transaction").toLocalDateTime());
        }
        order.setEmployeeId(rs.getInt("employee_id"));
        order.setWaiter(rs.getString("waiter"));
        order.setTerminal(rs.getString("terminal"));
        order.setPulledAt(rs.getTimestamp("pulled_at").toLocalDateTime());

        if (rs.getTimestamp("created_date") != null) {
            order.setCreatedDate(rs.getTimestamp("created_date").toLocalDateTime());
        }

        if (rs.getTimestamp("modified_at") != null) {
            order.setCreatedDate(rs.getTimestamp("modified_at").toLocalDateTime());
        }
        order.setDataSource(rs.getInt("data_source"));
        if (rs.getDate("waste_date") != null) {
            order.setWasteDate(rs.getDate("waste_date").toLocalDate());
        }
        order.setStatusId(rs.getInt("status_id"));
        order.setOriginId(rs.getLong("origin_id"));


        order.getOrderItems().add(orderItemRowMapper.mapRow(rs,rowNum));

        return order;
    }
}
