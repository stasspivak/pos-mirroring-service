package com.weissbeerger.mappers;

import com.weissbeerger.model.OrderItem;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class OrderItemRowMapper implements RowMapper<OrderItem>{

    public OrderItem mapRow(ResultSet rs,int rowNum) throws SQLException {
        OrderItem orderItem = new OrderItem();
        orderItem.setId(rs.getLong("id"));
        orderItem.setItemId(rs.getInt("item_id"));
        orderItem.setOrderId(rs.getLong("order_id"));
        orderItem.setBarProductId(rs.getLong("bar_product_id"));
        orderItem.setQty(rs.getDouble("qty"));
        orderItem.setProductPrice(rs.getDouble("product_price"));
        orderItem.setProductSalePrice(rs.getDouble("product_sale_price"));
        orderItem.setTotalDiscount(rs.getDouble("total_discount"));
        orderItem.setTotalPriceIncTax(rs.getDouble("total_price_inc_tax"));
        orderItem.setTotalPrice(rs.getDouble("total_price"));


        if (rs.getTimestamp("time_ordered") != null) {
            orderItem.setTimeOrdered(rs.getTimestamp("time_ordered").toLocalDateTime());
        }
        orderItem.setTerminal(rs.getString("terminal"));
        orderItem.setWaiterCode(rs.getString("waiter_code"));
        orderItem.setIsCombo(rs.getString("is_combo"));
        return orderItem;
    }
}