package com.weissbeerger.mappers;

import com.weissbeerger.model.BarProduct;
import com.weissbeerger.model.PosProductBrand;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PosProductBrandMapper implements RowMapper<PosProductBrand> {

    public PosProductBrand mapRow(ResultSet rs,int rowNum) throws SQLException {
        PosProductBrand productBrand = new PosProductBrand();
        //(`product_id`, `brand_id`, `category_id`, `volume`)
        productBrand.setId(rs.getLong("id"));
        productBrand.setProductId(rs.getLong("product_id"));
        productBrand.setBrandId(rs.getLong("brand_id"));
        productBrand.setCategoryId(rs.getInt("category_id"));
        productBrand.setVolume(rs.getFloat("volume"));

        return productBrand;
    }
}
