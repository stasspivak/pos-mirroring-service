package com.weissbeerger.mappers;

import com.weissbeerger.model.BarProduct;
import com.weissbeerger.utils.Utils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class BarProductRowMapper implements RowMapper<BarProduct>{


    public BarProduct mapRow(ResultSet rs, int rowNum) throws SQLException {
        BarProduct bp = new BarProduct();
        bp.setId(rs.getLong("id"));
        bp.setTitle(rs.getString("title"));
        bp.setTitleOriginal(rs.getString("title_orig"));
        bp.setPrice(rs.getDouble("price"));
        bp.setCost(rs.getDouble("cost"));
        bp.setCode(rs.getString("code"));
        bp.setPosCode(rs.getString("pos_code"));
        bp.setCategoryId(rs.getInt("category_id"));
        bp.setCategory(rs.getString("pos_category_name"));
        bp.setPosBrandName(rs.getString("pos_brand_name"));
        bp.setVolume(rs.getDouble("pos_product_volume"));
        bp.setUnitsInPack(rs.getDouble("units_in_pack"));
        bp.setStoreNumber(rs.getInt("store_number"));
        bp.setBarcode(rs.getString("barcode"));
        bp.setBarId(rs.getInt("bar_id"));
        bp.setProductId(rs.getObject("product_id") != null ? rs.getInt("product_id") : null);
        bp.setMachineTranslation(rs.getString("machine_translation"));

        String timeStrInterted = (rs.getString("inserted_at"));
        if (timeStrInterted != null)
            bp.setInsertedAt(Utils.getLocalDateTime(timeStrInterted));
        String timeStrModified = (rs.getString("modified_at"));
        if (timeStrModified != null)
            bp.setModifiedAt(Utils.getLocalDateTime(timeStrModified));

        bp.setSource(rs.getInt("source"));
        return bp;
    }
}
