package com.weissbeerger.model;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;


// TODO: Auto-generated Javadoc

/**
 * The Class Order.
 */
@Document(collection = "PosOrders")
@Data
@JsonSerialize
public class Order {


    private Long id;

    private int barId;

    private String code;

    private LocalDateTime date;

    private LocalDate nd;


    private int hour;


    private String clientName = "";


    private int promo1Id = 0;


    private int promo2Id = 0;


    private int promo3Id = 0;


    private Double discount;

    private double tips;

    private double otherCharges;

    private double totalTaxableSales;


    private double totalUntaxableSales;


    private Double taxes;


    private double totalSales;

    private Double totalSalesIncTax;


    private String paymentType;


    private int seats;


    private String tableNumber;

    private LocalDate fiscalDate;

    private LocalDateTime timeOpen;
    private LocalDateTime timeOrdered;

    private LocalDateTime timeClose;

    private LocalDateTime timeSend;

    private LocalDateTime timePrint;

    private LocalDateTime timeTransaction;

    private Integer employeeId;


    private String waiter;


    private String terminal;

    private LocalDateTime pulledAt;

    private LocalDateTime createdDate;

    private LocalDateTime modifiedAt;

    private Integer dataSource;


    private LocalDate wasteDate;

    private int statusId;

    private Long originId;

    private String internalId;

    private List<OrderItem> orderItems = new ArrayList<>();


}
