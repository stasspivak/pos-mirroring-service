package com.weissbeerger.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "PosProduct")
@Data
public class PosProduct {

    private Long id;

    private String title;
    private String sku;
    private Long brandId;
    private Long servingSizeId;
    private Integer servingTypeId;
    private String packageSize;
    public PosProduct(){};

}
