package com.weissbeerger.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Objects;


@Data
@JsonSerialize
public class OrderItem {

    private Long id;

    private Integer itemId;


    private Long orderId;

    private Long barProductId;


    private double qty;


    private Double productPrice;


    private Double productSalePrice;


    private Double totalDiscount;


    private Double totalPrice;


    private Double totalPriceIncTax;


    private LocalDateTime timeOrdered;


    private String terminal;


    private String waiterCode;

    private String isCombo;


}
