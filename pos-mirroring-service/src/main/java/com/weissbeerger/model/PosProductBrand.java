package com.weissbeerger.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "PosProdcutBrand")
@Data
public class PosProductBrand {

    Long id;

    private Long productId;

    private Long brandId;

    private Integer categoryId;

    private Float volume;

    public PosProductBrand() {};


}