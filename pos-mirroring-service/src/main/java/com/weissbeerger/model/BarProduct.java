package com.weissbeerger.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Comparator;

@Document(collection = "PosBarProducts")
@Data
@JsonSerialize
public class BarProduct {


    private Long id;


    private String title;


    private String titleOriginal;

    private Double price = null;
    private Double cost = null;

    private String code;


    private String posCode;

    private Integer categoryId;


    private String category;

    private String posBrandName;

    private Integer storeNumber = null;
    private String barcode = "";

    private int barId;

    private Integer productId = 0;

    private String machineTranslation;

    private LocalDateTime insertedAt;

    private Double volume = null;

    private Double unitsInPack = null;

    private LocalDateTime modifiedAt;

    private Integer source = 0;


}
