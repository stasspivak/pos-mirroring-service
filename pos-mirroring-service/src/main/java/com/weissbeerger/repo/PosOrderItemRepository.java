package com.weissbeerger.repo;

import com.weissbeerger.model.OrderItem;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PosOrderItemRepository extends MongoRepository<OrderItem, Long> {

        List<OrderItem> findByOrderId(int orderId);
}
