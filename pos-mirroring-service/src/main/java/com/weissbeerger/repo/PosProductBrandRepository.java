package com.weissbeerger.repo;

import com.weissbeerger.model.PosProductBrand;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PosProductBrandRepository extends MongoRepository<PosProductBrand, Long> {

    PosProductBrand findById(Long id);
}