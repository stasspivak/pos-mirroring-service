package com.weissbeerger.repo;

import com.weissbeerger.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface OrderRepository extends MongoRepository<Order, Long> {

    Order findById(Long id);

}
