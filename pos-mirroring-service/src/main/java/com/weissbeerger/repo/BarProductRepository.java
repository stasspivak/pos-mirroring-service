package com.weissbeerger.repo;

import com.weissbeerger.model.BarProduct;
import com.weissbeerger.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BarProductRepository extends MongoRepository<BarProduct, Long> {

    Order findById(Long id);
}
