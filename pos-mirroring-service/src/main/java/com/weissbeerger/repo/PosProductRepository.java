package com.weissbeerger.repo;

import com.weissbeerger.model.PosProduct;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PosProductRepository extends MongoRepository<PosProduct, Long> {

    PosProduct findById(Long id);
}
