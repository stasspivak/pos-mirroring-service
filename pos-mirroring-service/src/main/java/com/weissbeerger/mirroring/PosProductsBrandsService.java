package com.weissbeerger.mirroring;

import com.weissbeerger.model.PosProductBrand;
import com.weissbeerger.repo.PosProductBrandRepository;
import com.weissbeerger.criteria.MirroringCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Order(4)
@Service
public class PosProductsBrandsService implements MirroringCommand {


    public static final String SELECT_FROM_POS_PRODUCTS_BRANDS = "select * from `pos_products_brands` WHERE `product_id` >= :id1  AND `product_id` < :id2";


    @Autowired
    PosProductBrandRepository posProductBrandRepository;

    @Autowired
    private RowMapper<PosProductBrand> posProductBrandMapper;

    @Autowired
    @Qualifier("mySqlNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate mySqlNamedParameterJdbcTemplate;


    public void copyData(MirroringCriteria criteria) {


        try {

            String query = SELECT_FROM_POS_PRODUCTS_BRANDS;
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("id1", criteria.getIdStart());
            params.addValue("id2", criteria.getIdEnd());

            List<PosProductBrand> posProducts = mySqlNamedParameterJdbcTemplate.query(query, params, posProductBrandMapper);
            posProductBrandRepository.save(posProducts);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
