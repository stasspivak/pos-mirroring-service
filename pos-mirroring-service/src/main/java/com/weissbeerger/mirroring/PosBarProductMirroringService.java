package com.weissbeerger.mirroring;

import com.weissbeerger.model.BarProduct;
import com.weissbeerger.repo.BarProductRepository;
import com.weissbeerger.criteria.MirroringCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Order(2)
@Service
public class PosBarProductMirroringService implements MirroringCommand {


    public static final String SELECT_FROM_POS_BARS_PRODUCTS = "SELECT *  FROM pos_bars_products  WHERE `inserted_at`  <= :date1 AND `inserted_at` > :date2";


    @Autowired
    BarProductRepository barProductRepository;


    @Autowired
    @Qualifier("mySqlNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate mySqlNamedParameterJdbcTemplate;

    @Autowired
    private RowMapper<BarProduct> barProductRowMapper;


    public void copyData(MirroringCriteria criteria) {
        try {

            String query = SELECT_FROM_POS_BARS_PRODUCTS;
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("date1", criteria.getLastDate().format(DateTimeFormatter.ISO_DATE));
            params.addValue("date2", criteria.getLastDate().minusDays(1).format(DateTimeFormatter.ISO_DATE));

            List<BarProduct> barProducts = mySqlNamedParameterJdbcTemplate.query(query, params, barProductRowMapper);
            barProductRepository.save(barProducts);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
