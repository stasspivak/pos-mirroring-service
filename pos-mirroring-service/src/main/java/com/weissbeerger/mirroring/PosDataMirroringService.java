package com.weissbeerger.mirroring;

import com.weissbeerger.repo.OrderRepository;
import com.weissbeerger.criteria.MirroringCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.*;

@Order(1)
@Service
public class PosDataMirroringService implements MirroringCommand {


    public static final String SELECT_FROM_POS_ORDERS_ORDER_INNER_JOIN_POS_ORDERS_ITEMS_ITEMS = "select * from `pos_orders` `order` INNER JOIN `pos_ordersItems` `items` on `order`.`id` = items.`order_id` where `order`.`nd` <= :date1 AND `order`.`nd` > :date2 order by date LIMIT :start,:count ;";
    public static final String COUNT_QUERY = "select count(*) from `pos_orders` `order` INNER JOIN `pos_ordersItems` `items` on `order`.`id` = items.`order_id` where `order`.`nd` <= :date1 AND `order`.`nd` > :date2 order by date;";


    @Autowired
    private RowMapper<com.weissbeerger.model.Order> orderRowMapper;

    @Value("${posdata.mirroring.bulksize}")
    private int posMirroringBulkSize;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    @Qualifier("mySqlNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate mySqlNamedParameterJdbcTemplate;

    public void copyData(MirroringCriteria criteria) {

        int bulkSize = posMirroringBulkSize;
        MapSqlParameterSource countParams = new MapSqlParameterSource();
        countParams.addValue("date1", criteria.getLastDate().format(DateTimeFormatter.ISO_DATE));
        countParams.addValue("date2", criteria.getLastDate().minusDays(1).format(DateTimeFormatter.ISO_DATE));
        int count = mySqlNamedParameterJdbcTemplate.queryForObject(COUNT_QUERY, countParams, (rs, rowNum) -> rs.getInt(1));

        for (int i = 0; i < count; i = i + bulkSize) {
            int startPram = i;
            int countPram = i + bulkSize;

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("date1", criteria.getLastDate().format(DateTimeFormatter.ISO_DATE));
            params.addValue("date2", criteria.getLastDate().minusDays(1).format(DateTimeFormatter.ISO_DATE));
            params.addValue("start", startPram);
            params.addValue("count", countPram);


            List<com.weissbeerger.model.Order> posOrders = mySqlNamedParameterJdbcTemplate.query(SELECT_FROM_POS_ORDERS_ORDER_INNER_JOIN_POS_ORDERS_ITEMS_ITEMS, params, orderRowMapper);

            Map<Long, com.weissbeerger.model.Order> aggregatedOrdersMap = aggregateOrderItems(posOrders);

            orderRepository.save(aggregatedOrdersMap.values());
            System.out.println(i + " is finishing");
        }
    }

    private Map<Long, com.weissbeerger.model.Order> aggregateOrderItems(List<com.weissbeerger.model.Order> posOrders) {
        Map<Long, com.weissbeerger.model.Order> orderHashMap = new HashMap<>();
        for (com.weissbeerger.model.Order order : posOrders) {
            if (orderHashMap.get(order.getId()) != null) {
                com.weissbeerger.model.Order exitingOrder = orderHashMap.get(order.getId());
                exitingOrder.getOrderItems().addAll(order.getOrderItems());
            } else {
                orderHashMap.put(order.getId(), order);
            }
        }
        return orderHashMap;
    }
}
