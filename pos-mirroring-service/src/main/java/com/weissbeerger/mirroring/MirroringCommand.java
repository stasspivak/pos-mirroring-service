package com.weissbeerger.mirroring;

import com.weissbeerger.criteria.MirroringCriteria;

public interface MirroringCommand {

    void copyData(MirroringCriteria criteria);
}
