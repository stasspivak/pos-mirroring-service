package com.weissbeerger.mirroring;

import com.weissbeerger.model.PosProduct;
import com.weissbeerger.repo.PosProductRepository;
import com.weissbeerger.criteria.MirroringCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Order(3)
@Service
public class PosProductMirroringService  implements  MirroringCommand {


    public static final String SELECT_FROM_POS_PRODUCTS = "select * from `pos_products` WHERE `id` >=  :id1 AND `id` < :id2";

    @Autowired
    PosProductRepository posProductRepository;

    @Autowired
    private RowMapper<PosProduct> posProductMapper;

    @Autowired
    @Qualifier("mySqlNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate mySqlNamedParameterJdbcTemplate;

    public void copyData(MirroringCriteria criteria){


        try {

            String query = SELECT_FROM_POS_PRODUCTS;
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("id1", criteria.getIdStart());
            params.addValue("id2", criteria.getIdEnd());

            List<PosProduct> posProducts = mySqlNamedParameterJdbcTemplate.query(query, params,posProductMapper);
            posProductRepository.save(posProducts);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
