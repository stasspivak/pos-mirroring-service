package com.weissbeerger.controllers;

import com.weissbeerger.mirroring.MirroringCommand;
import com.weissbeerger.criteria.MirroringCriteria;
import com.weissbeerger.threads.DaemonThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
public class MirroringController {

    @Autowired
    private List<MirroringCommand> mirroringCommands;

    @Value("${posdata.mirroring.workers.count}")
    private int posDataThreadsCount;

    @Autowired
    private DaemonThreadFactory daemonThreadFactory;

    @JmsListener(destination = "${posdata.mirroring.queue}")
    public void consumeMessage(String message) throws Exception {
        System.out.println(message);
        MirroringCriteria criteria = new MirroringCriteria();
        criteria.setIdStart(0);
        criteria.setIdEnd(250);
        criteria.setLastDate(LocalDate.of(2017, 12, 31));
        ExecutorService executor = Executors.newFixedThreadPool(posDataThreadsCount,daemonThreadFactory);
        for (MirroringCommand command : mirroringCommands) {
            executor.execute(() ->
                    {
                        System.out.println("Working thread name "+Thread.currentThread().getName());
                        command.copyData(criteria);
                    }
            );
        }

    }

}

