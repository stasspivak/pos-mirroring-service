package com.weissbeerger.controllers;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.weissbeerger.model.Order;
import com.weissbeerger.model.OrderItem;
import com.weissbeerger.repo.OrderRepository;
import com.weissbeerger.config.DatabaseConfig;
import com.weissbeerger.repo.PosOrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
public class MirroringRestController {

    @Autowired
    private DatabaseConfig config;

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    PosOrderItemRepository itemRepository;


    @Autowired
    private AmazonSQS amazonSQS;

    @Value("${posdata.mirroring.queue}")
    private String queueName;


    private SendMessageRequest createSendMessageRequest(String messageGroupId, String messageBody) {
        return new SendMessageRequest()
                .withQueueUrl(queueName)
                .withMessageGroupId(messageGroupId)
                .withMessageBody(messageBody);
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity mirrorOrders() {

        try {
            String messageGroupId = "MyGROUP_" + new Random().nextInt();

            SendMessageRequest sendMessageRequest = createSendMessageRequest(messageGroupId, "start");
            amazonSQS.sendMessage(sendMessageRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = "/postOrder", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity putOrder(@RequestBody Order order) {
        Order ord = orderRepository.save(order);
        return new ResponseEntity(HttpStatus.CREATED);

    }

    @RequestMapping(value = "/postItem", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity putOrderItems(@RequestBody List<OrderItem> posOrderItems) {
        itemRepository.save(posOrderItems);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/getOrder/{id}", method = RequestMethod.GET, produces = "application/json")
    public Order getOrder(@PathVariable Long id) {
        return orderRepository.findById(id);
    }

    @RequestMapping(value = "/getItems/{id}", method = RequestMethod.GET, produces = "application/json")
    public List<OrderItem> getOrderItems(@PathVariable int id) {
        return itemRepository.findByOrderId(id);
    }
}


